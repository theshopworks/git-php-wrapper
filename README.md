Git (PHP Wrapper)
================

A PHP wrapper for the Git version control system.

- [Installation](#installation)
- [Docker](#docker)
- [Code Style](#code-style)
- [Testing](#testing)
- [License](#license)

Installation
------------

Add the `git` package to your `composer.json` file.

``` json
{
    "require": {
        "theshopworks/git-php-wrapper": "^0.1"
    }
}
```

Or via the command line in the root of your project.

``` bash
$ composer require "theshopworks/git-php-wrapper:^0.1"
```

Docker
------

### Build local container

- `docker build -t git-php-wrapper .`

### Use script

- `docker run --rm -v $(pwd):/app -w /app git-php-wrapper`

### Using Composer

```
docker run --rm -v $(pwd):/app -w /app git-php-wrapper php /usr/bin/composer
```

## Code Style

```
docker run --rm -v $(pwd):/app -w /app git-php-wrapper php vendor/bin/php-cs-fixer fix --config=.php-cs-fixer.dist.php -v && docker run --rm -v $(pwd):/app -w /app git-php-wrapper php vendor/bin/phpcbf -p
```

## Tests

```
docker run --rm -v $(pwd):/app -w /app git-php-wrapper php vendor/bin/phpunit
```

Code Style
-------

This project follows the following code style guidelines:

- [PSR-2](http://www.php-fig.org/psr/psr-2/) & [PSR-4](http://www.php-fig.org/psr/psr-4/) coding style guidelines.
- Some chosen [PHP-CS-Fixer](https://github.com/FriendsOfPHP/PHP-CS-Fixer) rules.


``` bash
$ php vendor/bin/php-cs-fixer fix
```


Testing
-------

``` bash
$ php vendor/bin/phpunit
```

License
-------

The MIT License (MIT). Please see [License File](https://gitlab.com/theshopworks/git-php-wrapper/blob/master/LICENSE) for more information.
