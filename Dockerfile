FROM php:8.1-cli-alpine

RUN apk update \
    && apk add --no-cache \
        curl \
        git \
        libzip-dev \
        zip \
    && docker-php-ext-install zip

COPY --from=composer/composer:2-bin /composer /usr/bin/composer

WORKDIR /app

CMD ["php", "-a"]