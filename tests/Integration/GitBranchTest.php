<?php

declare(strict_types=1);

namespace Shopworks\Git\Tests\Integration;

use Faker\Factory;
use Mockery;
use OndraM\CiDetector\Ci\CiInterface;
use OndraM\CiDetector\CiDetector;
use Shopworks\Git\Commit\Commit;
use Shopworks\Git\Commit\CommitLogParser;
use Shopworks\Git\Commit\CommitParser;
use Shopworks\Git\File\File;
use Shopworks\Git\Process\Process;
use Shopworks\Git\Process\Processor;
use Shopworks\Git\Tests\GitTestCase;
use Shopworks\Git\Utility\CoreFunctions;
use Shopworks\Git\VersionControl\GitBranch;

class GitBranchTest extends GitTestCase
{
    /** @var GitBranch $gitBranch */
    private $gitBranch;
    private $topicBranchName;

    public function setUp(): void
    {
        parent::setUp();

        $this->topicBranchName = (Factory::create())->word;

        $this->runProcess(["touch", "master-file-a.txt"]);
        $this->runProcess([self::GIT_BINARY, "add", "master-file-a.txt"]);
        $this->runProcess([self::GIT_BINARY, "commit", "-m", "master commit a"]);
        $this->runProcess([self::GIT_BINARY, "checkout", "-b", $this->topicBranchName]);
        $this->runProcess(["touch", "test-branch-file-a.txt"]);
        $this->runProcess([self::GIT_BINARY, "add", "test-branch-file-a.txt"]);
        $this->runProcess([self::GIT_BINARY, "commit", "-m", "test branch commit a"]);
        $this->runProcess(["touch", "test-branch-file-b.txt"]);
        $this->runProcess([self::GIT_BINARY, "add", "test-branch-file-b.txt"]);
        $this->runProcess([self::GIT_BINARY, "commit", "-m", "test branch commit b"]);
        $this->runProcess([self::GIT_BINARY, "checkout", "master"]);
        $this->runProcess(["touch", "master-file-b.txt"]);
        $this->runProcess([self::GIT_BINARY, "add", "."]);
        $this->runProcess([self::GIT_BINARY, "commit", "-m", "master commit b"]);

        $this->gitBranch = new GitBranch(
            Mockery::mock(CiDetector::class, [
                'isCiDetected' => false,
            ]),
            $this->directory,
            new Processor(),
            new CommitLogParser(new CoreFunctions()),
            new CommitParser(new Processor(), new Process([]), $this->directory)
        );

        $this->assertEquals('master', $this->gitBranch->getName());
        $this->assertFalse($this->gitBranch->isDirty());
    }

    /**
     * @test
     */
    public function it_can_retrieve_branch_name(): void
    {
        $branchName = $this->gitBranch->getName();

        $this->assertEquals('master', $branchName);

        $this->checkoutBranch($this->topicBranchName);

        $branchName = $this->gitBranch->getName();

        $this->assertEquals($this->topicBranchName, $branchName);
    }

    /** @test */
    public function it_gets_the_branch_name_from_continuous_integration(): void
    {
        $gitBranch = new GitBranch(
            Mockery::mock(CiDetector::class, [
                'isCiDetected' => true,
                'detect' => Mockery::mock(CiInterface::class, [
                    'getBranch' => 'example-branch',
                ]),
            ]),
            $this->directory,
            new Processor(),
            new CommitLogParser(new CoreFunctions()),
            new CommitParser(new Processor(), new Process([]), $this->directory)
        );

        $this->assertEquals('remotes/origin/example-branch', $gitBranch->getName());
    }

    /**
     * @test
     */
    public function it_can_see_if_branch_is_dirty(): void
    {
        $this->runProcess(['touch', 'modified-file.txt']);

        $this->assertTrue($this->gitBranch->isDirty());

        $this->runProcess(['rm', 'modified-file.txt']);

        $this->assertFalse($this->gitBranch->isDirty());
    }

    /**
     * @test
     */
    public function it_can_get_parent_hash_at_pointer_to_master(): void
    {
        $masterCommitId = \trim($this->runProcess(
            [self::GIT_BINARY, "log", "--grep=master commit a", "--format=%H"]
        )->getOutput());

        $this->checkoutBranch($this->topicBranchName);

        $this->assertEquals($masterCommitId, $this->gitBranch->getParentHash());
    }

    /**
     * @test
     */
    public function it_can_get_all_changed_files_on_branch_including_uncommitted(): void
    {
        $this->checkoutBranch($this->topicBranchName);

        $changedFiles = $this->gitBranch->getChangedFiles();

        $this->assertCount(2, $changedFiles);

        $this->assertTrue($changedFiles->contains(function (File $file) {
            return $file->getName() === 'test-branch-file-a.txt' && $file->getExtension() === 'txt';
        }));

        $this->assertTrue($changedFiles->contains(function (File $file) {
            return $file->getName() === 'test-branch-file-b.txt' && $file->getExtension() === 'txt';
        }));
    }

    /** @test */
    public function it_gets_the_correct_parent_hash_for_a_branch_using_merge_commit_strategy_on_the_master_branch(): void
    {
        $this->runProcess(['/usr/bin/git', 'checkout', '-b', 'feature-commits-collection']);
        $branchName = $this->gitBranch->getName();
        $this->assertEquals('feature-commits-collection', $branchName);

        $this->runProcess(["touch", "feature-commits-file-a.txt"]);
        $this->runProcess([self::GIT_BINARY, "add", "."]);
        $this->runProcess([self::GIT_BINARY, "commit", "-m", "commit subject a", "-m", "Some body message also"]);
        $this->runProcess([self::GIT_BINARY, "checkout", "master"]);
        $this->runProcess([self::GIT_BINARY, "merge", "feature-commits-collection"]);

        $masterHash = \trim($this->runProcess(['/usr/bin/git', 'rev-parse', '--verify', 'HEAD'])->getOutput());

        $this->runProcess([self::GIT_BINARY, "checkout", "-b", "second-branch"]);
        $this->runProcess(["touch", "feature-commits-file-b.txt"]);
        $this->runProcess([self::GIT_BINARY, "add", "."]);
        $this->runProcess([self::GIT_BINARY, "commit", "-m", "commit subject b"]);

        $this->assertEquals($masterHash, $this->gitBranch->getParentHash());
    }

    /**
     * @test
     */
    public function it_gets_the_correct_parent_hash_for_a_branch_using_rebase_strategy_on_the_master_branch(): void
    {
        $this->runProcess(['/usr/bin/git', 'checkout', '-b', 'feature-commits-collection']);
        $this->runProcess(['/usr/bin/git', 'checkout', 'master']);
        $this->runProcess(['/usr/bin/git', 'commit', '-m', 'Empty commit', '--allow-empty']);
        $masterHash = \trim($this->runProcess(['/usr/bin/git', 'rev-parse', '--verify', 'HEAD'])->getOutput());
        $this->runProcess(['/usr/bin/git', 'checkout', 'feature-commits-collection']);
        $this->runProcess(['git', 'rebase', 'master']);
        $branchName = $this->gitBranch->getName();
        $this->assertEquals('feature-commits-collection', $branchName);

        $this->assertEquals($masterHash, $this->gitBranch->getParentHash());
    }

    /**
     * @test
     */
    public function it_can_determine_when_a_topic_branch_is_empty(): void
    {
        $this->runProcess(['/usr/bin/git', 'checkout', '-b', 'feature-commits-collection']);
        $this->runProcess(['/usr/bin/git', 'checkout', 'master']);
        $this->runProcess(['/usr/bin/git', 'commit', '-m', 'Empty commit', '--allow-empty']);
        $masterHash = \trim($this->runProcess(['/usr/bin/git', 'rev-parse', '--verify', 'HEAD'])->getOutput());

        $this->runProcess(['/usr/bin/git', 'checkout', 'feature-commits-collection']);
        $this->runProcess(['/usr/bin/git', 'rebase', 'master']);

        $branchName = $this->gitBranch->getName();
        $this->assertEquals('feature-commits-collection', $branchName);

        $this->assertTrue($this->gitBranch->isEmpty());
        $this->assertEquals($masterHash, $this->gitBranch->getParentHash());
    }

    /** @test */
    public function it_can_retrieve_a_collection_of_staged_changes_to_be_committed(): void
    {
        $this->runProcess(['/usr/bin/git', 'checkout', '-b', 'feature-commits-collection']);
        $this->runProcess(['touch', 'feature-commits-file-a.txt']);
        $this->runProcess(['/usr/bin/git', 'add', '.']);
        $this->runProcess(['touch', 'feature-commits-file-b.txt']);

        $files = $this->gitBranch->getStagedFiles();

        $this->assertCount(1, $files);

        $this->assertTrue($files->contains(function (File $file) {
            return $file->getName() === 'feature-commits-file-a.txt'
                && $file->getStatus() === 'A';
        }));
    }

    /** @test */
    public function it_can_get_a_collection_of_commits_on_a_topic_branch(): void
    {
        $this->runProcess([self::GIT_BINARY, "checkout", "-b", "feature-commits-collection"]);

        $branchName = $this->gitBranch->getName();

        $this->assertEquals("feature-commits-collection", $branchName);

        $this->runProcess(["touch", "feature-commits-file-a.txt"]);
        $this->runProcess([self::GIT_BINARY, "add", "."]);
        $this->runProcess([self::GIT_BINARY, "commit", "-m", "commit subject a", "-m", "Some body message also"]);
        $this->runProcess(['touch', 'feature-commits-file-b.txt']);
        $this->runProcess([self::GIT_BINARY, "add", "feature-commits-file-b.txt"]);
        $this->runProcess([self::GIT_BINARY, "commit", "-m", "Commit subject b"]);
        $this->runProcess(["touch", "feature-commits-file-c.txt"]);
        $this->runProcess([self::GIT_BINARY, "add", "."]);
        $this->runProcess([self::GIT_BINARY, "commit", "-m", "Commit message that is way too long"]);

        $actualCommitHashes = \explode(\PHP_EOL, $this->runProcess(
            [self::GIT_BINARY, "log", "master..feature-commits-collection", "--pretty=format:%H"]
        )->getOutput());

        $commitsOnBranch = $this->gitBranch->getCommits();

        $this->assertCount(3, $actualCommitHashes);
        $this->assertEquals(3, $commitsOnBranch->count());

        $commits = $commitsOnBranch->all();

        /** @var Commit $commitOne */
        /** @var Commit $commitTwo */
        /** @var Commit $commitThree */
        $commitOne = $commits[0];
        $commitTwo = $commits[1];
        $commitThree = $commits[2];

        $this->assertEquals($actualCommitHashes[2], $commitOne->getHash());
        $this->assertEquals('commit subject a', $commitOne->getCommitMessage()->getSubject());
        $this->assertEquals('Some body message also', $commitOne->getCommitMessage()->getBody());
        $this->assertEquals('Git', $commitOne->getAuthor()->getAuthorName());
        $this->assertEquals('testing@git.com', $commitOne->getAuthor()->getAuthorEmail());

        $this->assertEquals($actualCommitHashes[1], $commitTwo->getHash());
        $this->assertEquals('Commit subject b', $commitTwo->getCommitMessage()->getSubject());
        $this->assertEquals('', $commitTwo->getCommitMessage()->getBody());
        $this->assertEquals('Git', $commitTwo->getAuthor()->getAuthorName());
        $this->assertEquals('testing@git.com', $commitTwo->getAuthor()->getAuthorEmail());

        $this->assertEquals($actualCommitHashes[0], $commitThree->getHash());
        $this->assertEquals('Commit message that is way too long', $commitThree->getCommitMessage()->getSubject());
        $this->assertEquals('', $commitThree->getCommitMessage()->getBody());
        $this->assertEquals('Git', $commitThree->getAuthor()->getAuthorName());
        $this->assertEquals('testing@git.com', $commitThree->getAuthor()->getAuthorEmail());
    }

    private function checkoutBranch($branchName): void
    {
        $this->runProcess(['/usr/bin/git', 'checkout', $branchName]);
    }
}
