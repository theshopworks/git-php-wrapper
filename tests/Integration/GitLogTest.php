<?php

declare(strict_types=1);

namespace Shopworks\Tests\Integration;

use PHPUnit\Framework\Assert;
use Shopworks\Git\Commit\Commit;
use Shopworks\Git\Commit\CommitLogParser;
use Shopworks\Git\Commit\CommitParser;
use Shopworks\Git\Process\Process;
use Shopworks\Git\Process\Processor;
use Shopworks\Git\Tests\GitTestCase;
use Shopworks\Git\Utility\CoreFunctions;
use Shopworks\Git\VersionControl\GitLog;

class GitLogTest extends GitTestCase
{
    /** @var GitLog $gitBranch */
    private $gitLog;

    public function setUp(): void
    {
        parent::setUp();

        $this->gitLog = new GitLog(
            new Processor(),
            new CommitLogParser(new CoreFunctions()),
            new CommitParser(new Processor(), new Process([]), $this->directory)
        );
    }

    /** @test */
    public function it_can_get_a_log_between_two_given_reference_points(): void
    {
        $commitDate = "2021-02-01T22:13:13";
        $this->runProcess([self::GIT_BINARY, "commit", "--allow-empty", "-m", "Initial commit", "--date=$commitDate"], [
            "GIT_COMMITTER_DATE" => $commitDate,
        ]);
        $this->runProcess([self::GIT_BINARY, "checkout", "-b", "branch-A"]);
        $this->runProcess(["touch", "branch-a-file-a.txt"]);
        $this->runProcess([self::GIT_BINARY, "add", "branch-a-file-a.txt"]);

        $commitDate = "2021-02-01T23:12:45";
        $this->runProcess([self::GIT_BINARY, "commit", "-m", "branch a commit 1", "--date=$commitDate"], [
            "GIT_COMMITTER_DATE" => $commitDate,
        ]);

        $this->runProcess([self::GIT_BINARY, "checkout", "master"]);

        $commitDate = "2021-02-01T23:45:57";
        $this->runProcess([self::GIT_BINARY, "merge", "--no-ff", "branch-A", "-m", "Merge branch 'branch-A' into 'master'"], [
            "GIT_COMMITTER_DATE" => $commitDate,
            "GIT_AUTHOR_DATE" => $commitDate,
        ]);

        $commitStart = \trim($this->runProcess([self::GIT_BINARY, "log", "--grep=Merge branch 'branch-A' into 'master'", "--format=%H"])->getOutput());

        $this->runProcess([self::GIT_BINARY, "checkout", "-b", "branch-B"]);
        $this->runProcess(["touch", "branch-b-file-a.txt"]);
        $this->runProcess(["touch", "branch-b-file-b.txt"]);
        $this->runProcess([self::GIT_BINARY, "add", "."]);

        $commitDate = "2021-02-02T13:44:34";
        $this->runProcess([self::GIT_BINARY, "commit", "-m", "branch b commit 1", "--date=$commitDate"], [
            "GIT_COMMITTER_DATE" => $commitDate,
        ]);

        $this->runProcess(["touch", "branch-b-file-c.txt"]);
        $this->runProcess([self::GIT_BINARY, "add", "branch-b-file-c.txt"]);

        $commitDate = "2021-02-02T15:45:16";
        $this->runProcess([self::GIT_BINARY, "commit", "-m", "branch b commit 2", "--date=$commitDate"], [
            "GIT_COMMITTER_DATE" => $commitDate,
        ]);
        $this->runProcess([self::GIT_BINARY, "checkout", "master"]);

        $commitDate = "2021-02-03T14:22:32";
        $this->runProcess([self::GIT_BINARY, "merge", "--no-ff", "branch-B", "-m", "Merge branch 'branch-B' into 'master'"], [
            "GIT_COMMITTER_DATE" => $commitDate,
            "GIT_AUTHOR_DATE" => $commitDate,
        ]);

        $this->runProcess([self::GIT_BINARY, "checkout", "-b", "branch-C"]);
        $this->runProcess(["touch", "branch-c-file-a.txt"]);
        $this->runProcess([self::GIT_BINARY, "add", "branch-c-file-a.txt"]);

        $commitDate = "2021-02-04T08:55:23";
        $this->runProcess([self::GIT_BINARY, "commit", "-m", "branch c commit 1", "--date=$commitDate"], [
            "GIT_COMMITTER_DATE" => $commitDate,
        ]);
        $this->runProcess(["touch", "branch-c-file-b.txt"]);
        $this->runProcess([self::GIT_BINARY, "add", "branch-c-file-b.txt"]);

        $commitDate = "2021-02-05T01:23:53";
        $this->runProcess([self::GIT_BINARY, "commit", "-m", "branch c commit 2", "--date=$commitDate"], [
            "GIT_COMMITTER_DATE" => $commitDate,
        ]);
        $this->runProcess([self::GIT_BINARY, "checkout", "master"]);

        $commitDate = "2021-02-06T00:13:13";
        $this->runProcess([self::GIT_BINARY, "merge", "--no-ff", "branch-C", "-m", "Merge branch 'branch-C' into 'master'"], [
            "GIT_COMMITTER_DATE" => $commitDate,
            "GIT_AUTHOR_DATE" => $commitDate,
        ]);

        $commitEnd = \trim($this->runProcess([self::GIT_BINARY, "log", "--grep=Merge branch 'branch-C' into 'master'", "--format=%H"])->getOutput());
        $mergesLogActual = $this->gitLog->getCommitsInRange($commitStart, $commitEnd, "--first-parent master --merges");

        Assert::assertCount(2, $mergesLogActual);

        $commits = $mergesLogActual->all();

        /** @var Commit $commitOne */
        /** @var Commit $commitTwo */
        $commitOne = $commits[0];
        $commitTwo = $commits[1];

        Assert::assertEquals("Merge branch 'branch-C' into 'master'", $commitOne->getCommitMessage()->getSubject());
        Assert::assertEquals("", $commitOne->getCommitMessage()->getBody());
        Assert::assertEquals("Git", $commitOne->getAuthor()->getAuthorName());
        Assert::assertEquals("testing@git.com", $commitOne->getAuthor()->getAuthorEmail());

        Assert::assertEquals("Merge branch 'branch-B' into 'master'", $commitTwo->getCommitMessage()->getSubject());
        Assert::assertEquals("", $commitTwo->getCommitMessage()->getBody());
        Assert::assertEquals("Git", $commitTwo->getAuthor()->getAuthorName());
        Assert::assertEquals("testing@git.com", $commitTwo->getAuthor()->getAuthorEmail());

        $noMergesActual = $this->gitLog->getCommitsInRange($commitStart, $commitEnd, "--no-merges");

        Assert::assertCount(4, $noMergesActual);

        $commits = $noMergesActual->all();

        /** @var Commit $commitOne */
        /** @var Commit $commitTwo */
        /** @var Commit $commitThree */
        /** @var Commit $commitFour */
        $commitOne = $commits[0];
        $commitTwo = $commits[1];
        $commitThree = $commits[2];
        $commitFour = $commits[3];

        Assert::assertEquals("branch c commit 2", $commitOne->getCommitMessage()->getSubject());
        Assert::assertEquals("", $commitOne->getCommitMessage()->getBody());
        Assert::assertEquals("Git", $commitOne->getAuthor()->getAuthorName());
        Assert::assertEquals("testing@git.com", $commitOne->getAuthor()->getAuthorEmail());

        Assert::assertEquals("branch c commit 1", $commitTwo->getCommitMessage()->getSubject());
        Assert::assertEquals("", $commitTwo->getCommitMessage()->getBody());
        Assert::assertEquals("Git", $commitTwo->getAuthor()->getAuthorName());
        Assert::assertEquals("testing@git.com", $commitTwo->getAuthor()->getAuthorEmail());

        Assert::assertEquals("branch b commit 2", $commitThree->getCommitMessage()->getSubject());
        Assert::assertEquals("", $commitThree->getCommitMessage()->getBody());
        Assert::assertEquals("Git", $commitThree->getAuthor()->getAuthorName());
        Assert::assertEquals("testing@git.com", $commitThree->getAuthor()->getAuthorEmail());

        Assert::assertEquals("branch b commit 1", $commitFour->getCommitMessage()->getSubject());
        Assert::assertEquals("", $commitFour->getCommitMessage()->getBody());
        Assert::assertEquals("Git", $commitFour->getAuthor()->getAuthorName());
        Assert::assertEquals("testing@git.com", $commitFour->getAuthor()->getAuthorEmail());
    }
}
