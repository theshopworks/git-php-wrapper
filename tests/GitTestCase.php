<?php

declare(strict_types=1);

namespace Shopworks\Git\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Process\Process;

class GitTestCase extends TestCase
{
    protected const GIT_BINARY = '/usr/bin/git';
    protected $directory;
    protected $testFileName;

    public function setUp(): void
    {
        $this->directory = \sys_get_temp_dir() . '/git-tests/';

        if (!\is_dir($this->directory)) {
            \mkdir($this->directory, 0755, true);
        } else {
            $this->runProcess(['rm', '-rf',  \sys_get_temp_dir() . '/git-tests' . \DIRECTORY_SEPARATOR . '*']);
        }

        $this->directory = \realpath($this->directory);
        $this->testFileName = 'test.txt';

        \chdir($this->directory);
        $this->runProcess(["/usr/bin/git", "init"]);
        $this->runProcess(["/usr/bin/git", "config", "user.name", "Git"]);
        $this->runProcess(["/usr/bin/git", "config", "user.email", "testing@git.com"]);
    }

    public function tearDown(): void
    {
        $this->runProcess(['rm', '-rf', \sys_get_temp_dir() . '/git-tests/']);

        \Mockery::close();

        parent::tearDown();
    }

    protected function runProcess(array $command, array $envVariables = []): Process
    {
        $process = new Process($command, $this->directory, $envVariables);
        $process->run();

        return $process;
    }
}
