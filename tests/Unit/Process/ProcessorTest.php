<?php

declare(strict_types=1);

namespace Shopworks\Git\Tests\Unit\Process;

use Shopworks\Git\Process\Process;
use Shopworks\Git\Process\Processor;
use Shopworks\Git\Tests\UnitTestCase;

class ProcessorTest extends UnitTestCase
{
    private Processor $processor;

    public function setUp(): void
    {
        parent::setUp();

        $this->processor = new Processor();
    }

    /**
     * @test
     */
    public function it_can_run_a_process(): void
    {
        $process = \Mockery::mock(Process::class);
        $process->shouldReceive('setTimeout')->with(3600)->once();
        $process->shouldReceive('run')->once();

        $this->assertInstanceOf(Process::class, $this->processor->process($process));
    }
}
