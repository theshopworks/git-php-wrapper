<?php

namespace Shopworks\Git\Tests\Unit\Commit;

use Illuminate\Support\Collection;
use PHPUnit\Framework\Assert;
use Shopworks\Git\Commit\Author;
use Shopworks\Git\Commit\Commit;
use Shopworks\Git\Commit\CommitCollection;
use Shopworks\Git\File\File;
use Shopworks\Git\File\FileCollection;
use Shopworks\Git\Tests\UnitTestCase;
use Shopworks\Git\VersionControl\DiffStatus;

class CommitCollectionTest extends UnitTestCase
{
    /** @test */
    public function it_can_return_the_commit_count(): void
    {
        $commitCollection = new CommitCollection([
            \Mockery::mock(Commit::class),
            \Mockery::mock(Commit::class),
            \Mockery::mock(Commit::class),
            \Mockery::mock(Commit::class),
        ]);

        $this->assertEquals(4, $commitCollection->count());
    }

    /** @test */
    public function it_can_return_all_of_the_commits_in_the_collection(): void
    {
        $collection = [\Mockery::mock(Commit::class)];
        $commitCollection = new CommitCollection($collection);

        $this->assertEquals($collection, $commitCollection->all());
    }

    /** @test */
    public function it_can_return_the_unique_author_names_in_the_collection(): void
    {
        $collection = [
            \Mockery::mock(Commit::class, [
                'getAuthor' => \Mockery::mock(Author::class, [
                    'getAuthorName' => 'Optimus Prime',
                ]),
            ]),
            \Mockery::mock(Commit::class, [
                'getAuthor' => \Mockery::mock(Author::class, [
                    'getAuthorName' => 'Optimus Prime',
                ]),
            ]),
        ];
        $commitCollection = new CommitCollection($collection);

        $this->assertEquals(new Collection([
            'Optimus Prime',
        ]), $commitCollection->getAuthorNames());

        $collection = [
            \Mockery::mock(Commit::class, [
                'getAuthor' => \Mockery::mock(Author::class, [
                    'getAuthorName' => 'Optimus Prime',
                ]),
            ]),
            \Mockery::mock(Commit::class, [
                'getAuthor' => \Mockery::mock(Author::class, [
                    'getAuthorName' => 'Indianna Jones',
                ]),
            ]),
        ];
        $commitCollection = new CommitCollection($collection);

        $this->assertEquals(new Collection([
            'Optimus Prime',
            'Indianna Jones',
        ]), $commitCollection->getAuthorNames());
    }

    /** @test */
    public function it_can_detect_if_there_are_any_work_in_progress_commits_in_a_collection(): void
    {
        $collection = [
            \Mockery::mock(Commit::class, [
                'isWorkInProgress' => true,
            ]),
            \Mockery::mock(Commit::class, [
                'isWorkInProgress' => false,
            ]),
        ];
        $commitCollection = new CommitCollection($collection);

        $this->assertTrue($commitCollection->hasWorkInProgressCommits());

        $collection = [
            \Mockery::mock(Commit::class, [
                'isWorkInProgress' => false,
            ]),
            \Mockery::mock(Commit::class, [
                'isWorkInProgress' => false,
            ]),
        ];
        $commitCollection = new CommitCollection($collection);

        $this->assertFalse($commitCollection->hasWorkInProgressCommits());
    }

    /** @test */
    public function it_can_detect_if_there_are_any_fixup_commits_in_a_collection(): void
    {
        $collection = [
            \Mockery::mock(Commit::class, [
                'isFixup' => true,
            ]),
            \Mockery::mock(Commit::class, [
                'isFixup' => false,
            ]),
        ];
        $commitCollection = new CommitCollection($collection);

        $this->assertTrue($commitCollection->hasFixupCommits());

        $collection = [
            \Mockery::mock(Commit::class, [
                'isFixup' => false,
            ]),
            \Mockery::mock(Commit::class, [
                'isFixup' => false,
            ]),
        ];
        $commitCollection = new CommitCollection($collection);

        $this->assertFalse($commitCollection->hasFixupCommits());
    }

    /**
     * @dataProvider draftCommitsProvider
     * @test
     */
    public function it_can_detect_if_there_are_any_draft_commits_in_a_collection(bool $isFixUp, bool $isWorkInProgress): void
    {
        $collection = [
            \Mockery::mock(Commit::class, ['isFixup' => $isFixUp, 'isWorkInProgress' => $isWorkInProgress]),
        ];
        $commitCollection = new CommitCollection($collection);
        $this->assertTrue($commitCollection->hasDraftCommits());

        $collection = [
            \Mockery::mock(Commit::class, [
                'isFixup' => false,
                'isWorkInProgress' => false,
            ]),
        ];
        $commitCollection = new CommitCollection($collection);
        $this->assertFalse($commitCollection->hasDraftCommits());
    }

    public static function draftCommitsProvider(): array
    {
        return [
            [false, true],
            [true, false],
            [true, true],
        ];
    }

    /** @test */
    public function it_can_return_all_files_in_a_collection(): void
    {
        $fileCollectionOne = new FileCollection("/tmp");

        $files = [
            new DiffStatus("A", "tests/ExampleTest.php"),
            new DiffStatus("M", "tests/ExampleTest2.php"),
            new DiffStatus("D", "tests/RemoveThisTest.php"),
            new DiffStatus("R", "tests/ExampleTestRenamed.php", "tests/ExampleTest3.php"),
        ];

        $fileCollectionOne->addFiles($files);

        $fileCollectionTwo = new FileCollection("/tmp");

        $files = [
            new DiffStatus("A", "tests/AnotherFile.php"),
            new DiffStatus("M", "tests/AnotherFile2.php"),
            new DiffStatus("D", "tests/AnotherFileTest.php"),
            new DiffStatus("R", "tests/AnotherFileRenamed.php", "tests/AnotherFile2.php"),
        ];

        $fileCollectionTwo->addFiles($files);

        $fileCollectionThree = new FileCollection("/tmp");

        $files = [
            new DiffStatus("A", "tests/MoreFiles.php"),
            new DiffStatus("M", "tests/MoreFiles2.php"),
            new DiffStatus("D", "tests/MoreFilesTest.php"),
            new DiffStatus("R", "tests/MoreFilesRenamed.php", "tests/MoreFiles2.php"),
        ];

        $fileCollectionThree->addFiles($files);

        $collection = [
            \Mockery::mock(Commit::class, ['getFileCollection' => $fileCollectionOne]),
            \Mockery::mock(Commit::class, ['getFileCollection' => $fileCollectionTwo]),
            \Mockery::mock(Commit::class, ['getFileCollection' => $fileCollectionThree]),
        ];
        $commitCollection = CommitCollection::make($collection);

        $actual = $commitCollection->getFiles();

        Assert::assertCount(12, $actual);
        /** @var File $fileOne */
        $fileOne = $actual[0];
        Assert::assertInstanceOf(File::class, $fileOne);
        Assert::assertEquals("tests/ExampleTest.php", $fileOne->getRelativePath());
        Assert::assertEquals("A", $fileOne->getStatus());
        Assert::assertNull($fileOne->getFilePathBeforeRename());

        /** @var File $fileTwo */
        $fileTwo = $actual[1];
        Assert::assertInstanceOf(File::class, $fileTwo);
        Assert::assertEquals("tests/ExampleTest2.php", $fileTwo->getRelativePath());
        Assert::assertEquals("M", $fileTwo->getStatus());
        Assert::assertNull($fileTwo->getFilePathBeforeRename());

        /** @var File $fileThree */
        $fileThree = $actual[2];
        Assert::assertInstanceOf(File::class, $fileThree);
        Assert::assertEquals("tests/RemoveThisTest.php", $fileThree->getRelativePath());
        Assert::assertEquals("D", $fileThree->getStatus());

        /** @var File $fileFour */
        $fileFour = $actual[3];
        Assert::assertInstanceOf(File::class, $fileFour);
        Assert::assertEquals("tests/ExampleTestRenamed.php", $fileFour->getRelativePath());
        Assert::assertEquals("R", $fileFour->getStatus());

        /** @var File $fileFive */
        $fileFive = $actual[4];
        Assert::assertInstanceOf(File::class, $fileFive);
        Assert::assertEquals("tests/AnotherFile.php", $fileFive->getRelativePath());
        Assert::assertEquals("A", $fileFive->getStatus());
        Assert::assertNull($fileFive->getFilePathBeforeRename());

        /** @var File $fileSix */
        $fileSix = $actual[5];
        Assert::assertInstanceOf(File::class, $fileSix);
        Assert::assertEquals("tests/AnotherFile2.php", $fileSix->getRelativePath());
        Assert::assertEquals("M", $fileSix->getStatus());
        Assert::assertNull($fileSix->getFilePathBeforeRename());

        /** @var File $fileSeven */
        $fileSeven = $actual[6];
        Assert::assertInstanceOf(File::class, $fileSeven);
        Assert::assertEquals("tests/AnotherFileTest.php", $fileSeven->getRelativePath());
        Assert::assertEquals("D", $fileSeven->getStatus());

        /** @var File $fileEight */
        $fileEight = $actual[7];
        Assert::assertInstanceOf(File::class, $fileEight);
        Assert::assertEquals("tests/AnotherFileRenamed.php", $fileEight->getRelativePath());
        Assert::assertEquals("R", $fileEight->getStatus());

        /** @var File $fileNine */
        $fileNine = $actual[8];
        Assert::assertInstanceOf(File::class, $fileNine);
        Assert::assertEquals("tests/MoreFiles.php", $fileNine->getRelativePath());
        Assert::assertEquals("A", $fileNine->getStatus());
        Assert::assertNull($fileNine->getFilePathBeforeRename());

        /** @var File $fileEleven */
        $fileEleven = $actual[9];
        Assert::assertInstanceOf(File::class, $fileEleven);
        Assert::assertEquals("tests/MoreFiles2.php", $fileEleven->getRelativePath());
        Assert::assertEquals("M", $fileEleven->getStatus());
        Assert::assertNull($fileEleven->getFilePathBeforeRename());

        /** @var File $fileTwelve */
        $fileTwelve = $actual[10];
        Assert::assertInstanceOf(File::class, $fileTwelve);
        Assert::assertEquals("tests/MoreFilesTest.php", $fileTwelve->getRelativePath());
        Assert::assertEquals("D", $fileTwelve->getStatus());

        /** @var File $fileThirteen */
        $fileThirteen = $actual[11];
        Assert::assertInstanceOf(File::class, $fileThirteen);
        Assert::assertEquals("tests/MoreFilesRenamed.php", $fileThirteen->getRelativePath());
        Assert::assertEquals("R", $fileThirteen->getStatus());
    }
}
