<?php

namespace Shopworks\Git\Tests\Unit\Commit;

use Mockery;
use Shopworks\Git\Commit\Author;
use Shopworks\Git\Commit\Commit;
use Shopworks\Git\Commit\Message;
use Shopworks\Git\File\FileCollection;
use Shopworks\Git\Tests\UnitTestCase;

class CommitTest extends UnitTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @dataProvider workInProgressProvider
     * @test
     */
    public function it_can_detect_if_commit_is_work_in_progress(string $commitSubject): void
    {
        $exampleOne = new Commit(
            '123456',
            ['654321'],
            new Message($commitSubject, ''),
            Mockery::mock(Author::class),
            Mockery::mock(FileCollection::class)
        );

        $this->assertTrue($exampleOne->isWorkInProgress());

        $nonWipCommit = new Commit(
            '123456',
            ['654321'],
            new Message("Wipe database", ''),
            Mockery::mock(Author::class),
            Mockery::mock(FileCollection::class)
        );

        $this->assertFalse($nonWipCommit->isWorkInProgress());
    }

    /** @test */
    public function it_can_detect_if_commit_is_a_fixup_commit(): void
    {
        $exampleOne = new Commit(
            '123456',
            ['654321'],
            new Message("fixup! another commit", ''),
            Mockery::mock(Author::class),
            Mockery::mock(FileCollection::class)
        );

        $this->assertTrue($exampleOne->isFixup());

        $nonFixupCommit = new Commit(
            '123456',
            ['654321'],
            new Message("Fixup the commits, dude!", ''),
            Mockery::mock(Author::class),
            Mockery::mock(FileCollection::class)
        );

        $this->assertFalse($nonFixupCommit->isFixup());
    }

    public static function workInProgressProvider(): array
    {
        return [
            ['wip'],
            ['Wip'],
            ['wIp'],
            ['wiP'],
            ['wIP'],
            ['WIp'],
            ['WIP'],
            ['WIP some text'],
            ['WIPppp'],
        ];
    }
}
