<?php

namespace Shopworks\Git\Tests\Unit\Commit;

use Illuminate\Support\Collection;
use Mockery\MockInterface;
use Shopworks\Git\Commit\CommitLogParser;
use Shopworks\Git\Tests\UnitTestCase;
use Shopworks\Git\Utility\CoreFunctions;

class CommitLogParserTest extends UnitTestCase
{
    /** @test */
    public function it_can_separate_commits_in_log_using_a_unique_string(): void
    {
        $commitLog = <<<'EOT'
Hash: a6e77b8f6686a6b97cd5cf27ad1cc3f6894edc0d
Parents: 4385b4ee83c6afb85434eb9aab36f94a347738b5
Subject: ___SHOPWORKS_GIT___123456123456
Author Name: ___SHOPWORKS_GIT___123456
Author Date: Thu, 29 Nov 2018 16:24:26 +0000
Author Email: testing@git.com
Committer Name: Git
Committer Date: Thu, 29 Nov 2018 16:24:26 +0000
Committer Email: testing@git.com
Name Status:
A	feature-commits-file-a.txt

Hash: 16af42e349084e5fce07c11ddba6da0274250091
Parents: a6e77b8f6686a6b97cd5cf27ad1cc3f6894edc0d
Subject: Commit subject b
Author Name: Git
Author Date: Thu, 29 Nov 2018 16:24:26 +0000
Author Email: testing@git.com
Committer Name: Git
Committer Date: Thu, 29 Nov 2018 16:24:26 +0000
Committer Email: testing@git.com
Name Status:
M	feature-commits-file-a.txt

Hash: 53e5efa5c11eee1eb66f2f671dcfd018ebacc0bb
Parents: 16af42e349084e5fce07c11ddba6da0274250091
Subject: Commit message that is way too long
Author Name: Git
Author Date: Thu, 29 Nov 2018 16:24:26 +0000
Author Email: testing@git.com
Committer Name: Git
Committer Date: Thu, 29 Nov 2018 16:24:26 +0000
Committer Email: testing@git.com
Name Status:
A	feature-commits-file-b.txt
EOT;
        /** @var MockInterface|CoreFunctions $coreFunctions */
        $logParser = new CommitLogParser($coreFunctions = \Mockery::mock(CoreFunctions::class));
        $coreFunctions->shouldReceive('generateRandomNumber')->times(3)->andReturn(123456);
        $actual = $logParser->parse($commitLog);

        $commitOne = <<<'EOT'

Hash: a6e77b8f6686a6b97cd5cf27ad1cc3f6894edc0d
Parents: 4385b4ee83c6afb85434eb9aab36f94a347738b5
Subject: ___SHOPWORKS_GIT___123456123456
Author Name: ___SHOPWORKS_GIT___123456
Author Date: Thu, 29 Nov 2018 16:24:26 +0000
Author Email: testing@git.com
Committer Name: Git
Committer Date: Thu, 29 Nov 2018 16:24:26 +0000
Committer Email: testing@git.com
Name Status:
A	feature-commits-file-a.txt

EOT;

        $commitTwo = <<<'EOT'

Hash: 16af42e349084e5fce07c11ddba6da0274250091
Parents: a6e77b8f6686a6b97cd5cf27ad1cc3f6894edc0d
Subject: Commit subject b
Author Name: Git
Author Date: Thu, 29 Nov 2018 16:24:26 +0000
Author Email: testing@git.com
Committer Name: Git
Committer Date: Thu, 29 Nov 2018 16:24:26 +0000
Committer Email: testing@git.com
Name Status:
M	feature-commits-file-a.txt

EOT;

        $commitThree = <<<'EOT'

Hash: 53e5efa5c11eee1eb66f2f671dcfd018ebacc0bb
Parents: 16af42e349084e5fce07c11ddba6da0274250091
Subject: Commit message that is way too long
Author Name: Git
Author Date: Thu, 29 Nov 2018 16:24:26 +0000
Author Email: testing@git.com
Committer Name: Git
Committer Date: Thu, 29 Nov 2018 16:24:26 +0000
Committer Email: testing@git.com
Name Status:
A	feature-commits-file-b.txt
EOT;

        $this->assertEquals(new Collection([
            $commitOne,
            $commitTwo,
            $commitThree,
        ]), $actual);
    }
}
