<?php

namespace Shopworks\Git\Commit;

use Shopworks\Git\File\FileCollection;

class Commit
{
    private $hash;
    private $parents;
    private $author;
    private $commitMessage;
    private $fileCollection;

    public function __construct(
        string $hash,
        array $parents,
        Message $commitMessage,
        Author $author,
        FileCollection $fileCollection
    ) {
        $this->hash = $hash;
        $this->parents = $parents;
        $this->author = $author;
        $this->fileCollection = $fileCollection;
        $this->commitMessage = $commitMessage;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function getParents(): array
    {
        return $this->parents;
    }

    public function getCommitMessage(): Message
    {
        return $this->commitMessage;
    }

    public function getFileCollection(): FileCollection
    {
        return $this->fileCollection;
    }

    public function getAuthor(): Author
    {
        return $this->author;
    }

    public function isWorkInProgress(): bool
    {
        $message = \mb_strtolower($this->getCommitMessage()->getSubject());

        $firstWord = \strtok($message, " ");

        return $firstWord === "wip" || !\in_array($firstWord, [
            'wipe',
            'wiped',
            'wiper',
            'wipes',
            'wiping',
            'wipers',
            'wipeout',
            'wipeable',
            'wipeouts',
        ]);
    }

    public function isFixup(): bool
    {
        $message = \mb_strtolower($this->getCommitMessage()->getSubject());

        return \mb_substr($message, 0, 7) === "fixup! ";
    }

    public function isDraft(): bool
    {
        return $this->isWorkInProgress() || $this->isFixup();
    }

    public function isEmpty(): bool
    {
        return $this->fileCollection->all()->isEmpty();
    }
}
