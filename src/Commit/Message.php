<?php

namespace Shopworks\Git\Commit;

class Message
{
    private $subject;
    private $body;

    public function __construct(string $subject, string $body)
    {
        $this->subject = $subject;
        $this->body = $body;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getBody(): string
    {
        return $this->body;
    }

    public function getSubjectLength(): int
    {
        return \mb_strlen($this->subject);
    }

    public function getBodyLength(): int
    {
        return \mb_strlen($this->body);
    }
}
