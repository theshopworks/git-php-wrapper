<?php

namespace Shopworks\Git\Commit;

use Shopworks\Git\File\File;
use Shopworks\Git\File\FileCollection;
use Shopworks\Git\Process\Process;
use Shopworks\Git\Process\Processor;

class CommitParser
{
    private $gitBinary;
    private $processor;
    private $currentWorkingDirectory;
    private $process;

    public function __construct(
        Processor $processor,
        Process $process,
        string $currentWorkingDirectory,
        string $gitBinary = '/usr/bin/git'
    ) {
        $this->processor = $processor;
        $this->process = $process;
        $this->gitBinary = $gitBinary;
        $this->currentWorkingDirectory = $currentWorkingDirectory;
    }

    public function parse(string $commit): Commit
    {
        [$commitDetails, $changedFiles] = $this->splitCommitMetaAndFileDetails($commit);

        $commitDetails = \explode(\PHP_EOL, \trim($commitDetails));
        $changedFiles = \explode(\PHP_EOL, \trim($changedFiles));
        $hash = $this->parseCommitHash($commitDetails[0]);
        $parents = $this->parseParentHashes($commitDetails[1]);
        $subject = $this->parseSubject($commitDetails[2]);
        $body = $this->getMessageBody($hash);

        return $this->createCommit($hash, $parents, $subject, $body, $commitDetails, $changedFiles);
    }

    private function splitCommitMetaAndFileDetails(string $commit): array
    {
        return \explode("Name Status:", $commit);
    }

    private function parseCommitHash(string $commitHash): string
    {
        return \mb_substr($commitHash, \mb_strlen("Hash: "));
    }

    private function parseParentHashes(string $commitDetails): array
    {
        return \explode(" ", \trim(\mb_substr($commitDetails, \mb_strlen("Parents: "))));
    }

    private function createCommit(
        string $hash,
        array $parents,
        string $subject,
        string $body,
        array $commitDetails,
        $changedFiles
    ): Commit {
        return new Commit(
            $hash,
            $parents,
            new Message($subject, $body),
            new Author(
                \mb_substr($commitDetails[3], \mb_strlen("Author Name: ")),
                \mb_substr($commitDetails[4], \mb_strlen("Author Date: ")),
                \mb_substr($commitDetails[5], \mb_strlen("Author Email: ")),
                \mb_substr($commitDetails[6], \mb_strlen("Committer Name: ")),
                \mb_substr($commitDetails[7], \mb_strlen("Committer Date: ")),
                \mb_substr($commitDetails[8], \mb_strlen("Committer Email: "))
            ),
            $this->getFilesCollection($changedFiles)
        );
    }

    private function parseSubject(string $commitDetails): string
    {
        return \mb_substr($commitDetails, \mb_strlen("Subject: "));
    }

    private function getMessageBody($hash): string
    {
        $command = $this->process->simple($this->gitBinary . " log -1 {$hash} --pretty=format:\"%b\"");

        return $this->processor->process($command)->getOutput();
    }

    private function getFilesCollection($changedFiles): FileCollection
    {
        $base = $this->getProjectBase();

        $files = new FileCollection($this->currentWorkingDirectory);

        if (empty($changedFiles)) {
            return $files;
        }

        foreach ($changedFiles as $file) {
            $fileData = \explode("\t", $file);
            $status = \reset($fileData);
            $relativePath = \end($fileData);

            $fullPath = \rtrim($base . \DIRECTORY_SEPARATOR . $relativePath);

            $file = new File($status, $fullPath, $base);
            $files->append($file);
        }

        return $files;
    }

    private function getProjectBase(): string
    {
        $process = $this->process->simple($this->gitBinary . ' rev-parse --show-toplevel');

        return $this->processor->process($process)->getOutput();
    }
}
