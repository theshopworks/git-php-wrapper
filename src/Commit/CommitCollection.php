<?php

namespace Shopworks\Git\Commit;

use Illuminate\Support\Collection;

class CommitCollection extends Collection
{
    public function getAuthorNames(): Collection
    {
        return $this->reduce(function (Collection $authors, Commit $commit) {
            return $authors->push($commit->getAuthor()->getAuthorName());
        }, collect())->unique();
    }

    public function getFiles(): Collection
    {
        return $this->reduce(function (Collection $files, Commit $commit) {
            return $files->merge($commit->getFileCollection()->all());
        }, collect())->unique();
    }

    public function hasWorkInProgressCommits(): bool
    {
        return $this->contains(function (Commit $commit) {
            return $commit->isWorkInProgress();
        });
    }

    public function hasFixupCommits(): bool
    {
        return $this->contains(function (Commit $commit) {
            return $commit->isFixup();
        });
    }

    public function hasDraftCommits(): bool
    {
        return $this->hasWorkInProgressCommits() || $this->hasFixupCommits();
    }
}
