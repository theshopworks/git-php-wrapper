<?php

declare(strict_types=1);

namespace Shopworks\Git\Process;

use Symfony\Component\Process\Process as SymfonyProcess;

class Process extends SymfonyProcess
{
    public const EXIT_CODE_SUCCESS = 0;
    public const EXIT_CODE_FAILED = 1;

    public function __construct(
        array $commandline,
        ?string $cwd = null,
        ?array $env = null,
        $input = null,
        float $timeout = 60
    ) {
        parent::__construct($commandline, $cwd, $env, $input, $timeout);
    }

    public static function simple(string $command): static
    {
        return static::fromShellCommandline($command);
    }

    public function getOutput(): string
    {
        return \trim(parent::getOutput());
    }
}
