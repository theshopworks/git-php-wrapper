<?php

declare(strict_types=1);

namespace  Shopworks\Git\VersionControl;

use Shopworks\Git\Commit\CommitCollection;
use Shopworks\Git\Commit\CommitLogParser;
use Shopworks\Git\Commit\CommitParser;
use Shopworks\Git\Process\Process;
use Shopworks\Git\Process\Processor;

class GitLog
{
    public const DEFAULT_FORMAT = "Hash: %H%nParents: %P%nSubject: %s%nAuthor Name: %an%nAuthor Date: "
        . "%aD%nAuthor Email: %ae%nCommitter Name: %cn%nCommitter Date: %cD%nCommitter Email: %ce%nName Status:";
    private string $gitBinary;
    private Processor $processor;
    private CommitParser $commitParser;
    private CommitLogParser $commitLogParser;

    public function __construct(
        Processor $processor,
        CommitLogParser $commitLogParser,
        CommitParser $commitParser,
        string $gitBinary = '/usr/bin/git'
    ) {
        $this->processor = $processor;
        $this->gitBinary = $gitBinary;
        $this->commitLogParser = $commitLogParser;
        $this->commitParser = $commitParser;
    }

    public function getCommitsInRange(
        string $start,
        string $end,
        string $logOptions = "",
        string $format = self::DEFAULT_FORMAT
    ): CommitCollection {
        $commits = $this->processor
            ->process(Process::simple(
                $this->gitBinary . " log $logOptions {$start}..{$end} --pretty=format:\"${format}\""
            ))
            ->getOutput();

        return $this->commitLogParser->parse($commits)
            ->reduce(function (CommitCollection $commits, string $item): CommitCollection {
                return $commits->push($this->commitParser->parse(\trim($item)));
            }, CommitCollection::make());
    }
}
