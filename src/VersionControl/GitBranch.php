<?php

declare(strict_types=1);

namespace Shopworks\Git\VersionControl;

use Illuminate\Support\Collection;
use OndraM\CiDetector\CiDetector;
use Shopworks\Git\Commit\CommitCollection;
use Shopworks\Git\Commit\CommitLogParser;
use Shopworks\Git\Commit\CommitParser;
use Shopworks\Git\File\FileCollection;
use Shopworks\Git\Process\Process;
use Shopworks\Git\Process\Processor;

class GitBranch
{
    private const REMOTES_ORIGIN = 'remotes/origin/';
    private $ciDetector;
    private $currentWorkingDirectory;
    private $gitBinary;
    private $processor;
    private $commitParser;
    private $commitLogParser;

    public function __construct(
        CiDetector $ciDetector,
        string $currentWorkingDirectory,
        Processor $processor,
        CommitLogParser $commitLogParser,
        CommitParser $commitParser,
        string $gitBinary = '/usr/bin/git'
    ) {
        $this->ciDetector = $ciDetector;
        $this->processor = $processor;
        $this->currentWorkingDirectory = $currentWorkingDirectory;
        $this->gitBinary = $gitBinary;
        $this->commitLogParser = $commitLogParser;
        $this->commitParser = $commitParser;
    }

    public function getName(): string
    {
        $process = Process::simple($this->gitBinary . ' symbolic-ref HEAD | sed -e "s/^refs\/heads\///"');

        return $this->ciDetector->isCiDetected() ?
            self::REMOTES_ORIGIN . $this->ciDetector->detect()->getBranch() :
            $this->processor->process($process)->getOutput();
    }

    public function getChangedFiles(): Collection
    {
        $fileCollection = new FileCollection($this->currentWorkingDirectory);

        $branchName = $this->getName();

        $process = Process::simple(
            $this->gitBinary . " log --name-status --pretty=format: {$this->getParentHash()}..${branchName}" .
            " | grep -E '^[A-Z0-9]{0,100}\\b' | sort | uniq"
        );

        $committedFilesProcess = $this->processor->process($process);

        if (!$committedFilesProcess->isSuccessful()) {
            return new Collection();
        }

        $fileCollection->addFiles(
            $this->parseDiff($committedFilesProcess->getOutput())
        );

        return $fileCollection->all();
    }

    public function getStagedFiles(): Collection
    {
        $fileCollection = new FileCollection($this->currentWorkingDirectory);

        $process = Process::simple(
            $this->gitBinary . " diff --cached --name-status"
        );

        $stagedFilesProcess = $this->processor->process($process);

        if (!$stagedFilesProcess->isSuccessful()) {
            return new Collection();
        }

        $fileCollection->addFiles(
            $this->parseDiff($stagedFilesProcess->getOutput())
        );

        return $fileCollection->all();
    }

    /**
     * A master branch using the merge commit strategy will return 2 parent hashes:
     *  - the ancestor (commit before the merge commit)
     *  - the actual parent we need in this case
     *
     * A master branch using the rebase strategy will usually have one parent hash.
     */
    public function getParentHash(): string
    {
        if ($this->isEmpty()) {
            return $this->getLatestCommitId();
        }

        $process = Process::simple(
            $this->gitBinary .
            " rev-list --boundary {$this->getName()}...{$this->getMasterBranch()} | grep '^-' | cut -c2-"
        );

        $hashes = $this->processor
            ->process($process)
            ->getOutput();

        $hashes = \explode(\PHP_EOL, $hashes);

        if (\count($hashes) === 1) {
            return $hashes[0];
        }

        return $hashes[1];
    }

    public function isDirty(): bool
    {
        $process = Process::simple($this->gitBinary . ' status --short');

        return !empty($this->processor->process($process)->getOutput());
    }

    public function isEmpty(): bool
    {
        $command = \trim($this->processor
            ->process(Process::simple(
                $this->gitBinary . " branch -a --contains {$this->getLatestCommitId()} | grep -E '(^|\\s)master$'"
            ))
            ->getOutput());

        return $command === 'master' || $command === self::REMOTES_ORIGIN . 'master';
    }

    public function getLatestCommitId(): string
    {
        return $this->processor
            ->process(Process::simple(
                $this->gitBinary . " rev-parse {$this->getName()}"
            ))
            ->getOutput();
    }

    public function getCommits(): CommitCollection
    {
        return $this->getCommitsOnBranch()
            ->reduce(function (CommitCollection $commits, string $item): CommitCollection {
                $commits->push($this->commitParser->parse(\trim($item)));

                return $commits;
            }, CommitCollection::make());
    }

    protected function getCommitsOnBranch(): Collection
    {
        $format = "Hash: %H%nParents: %P%nSubject: %s%nAuthor Name: %an%nAuthor Date: "
            . "%aD%nAuthor Email: %ae%nCommitter Name: %cn%nCommitter Date: %cD%nCommitter Email: %ce%nName Status:";

        $commits = $this->processor
            ->process(Process::simple(
                $this->gitBinary .
                " log --name-status --pretty=format:\"${format}\" {$this->getParentHash()}..{$this->getName()} "
                . "--reverse"
            ))
            ->getOutput();

        return $this->commitLogParser->parse($commits);
    }

    private function getMasterBranch()
    {
        return $this->ciDetector->isCiDetected() ? self::REMOTES_ORIGIN . 'master' : 'master';
    }

    private function parseDiff(string $gitOutput): array
    {
        $parser = new GitDiffParser();

        return $parser->parse($gitOutput);
    }
}
