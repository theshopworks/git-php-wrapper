<?php

declare(strict_types=1);

namespace Shopworks\Git\File;

use Illuminate\Support\Collection;
use Shopworks\Git\VersionControl\DiffStatus;

class FileCollection
{
    private $fileCollection;
    private $path;

    public function __construct($path)
    {
        $this->fileCollection = new Collection([]);
        $this->path = $path;
    }

    public function addFiles(array $diffStatus): void
    {
        /** @var DiffStatus $file */
        foreach ($diffStatus as $file) {
            $fullPath = \rtrim($this->path . \DIRECTORY_SEPARATOR . $file->getRelativePath());

            $file = new File($file->getStatus(), $fullPath, $this->path, $file->getRelativePathBeforeRename());
            $this->append($file);
        }
    }

    public function append(File $file): void
    {
        $this->fileCollection->push($file);
    }

    public function all(): Collection
    {
        return $this->fileCollection;
    }
}
