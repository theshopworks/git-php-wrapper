<?php

namespace Shopworks\Git\File;

class File
{
    public const STATUS_ADDED = 'A';
    public const STATUS_COPIED = 'C';
    public const STATUS_MODIFIED = 'M';
    public const STATUS_RENAMED = 'R';
    public const STATUS_DELETED = 'D';
    public const STATUS_UNSTAGED = '??';

    protected $filePath;
    protected $fileStatus;
    protected $projectPath;
    protected $cachedPath;
    protected $renamedFilePath;

    public function __construct(
        string $fileStatus,
        string $filePath,
        string $projectPath,
        ?string $renamedFilePath = null
    ) {
        $this->fileStatus = $fileStatus;
        $this->filePath = $filePath;
        $this->projectPath = \rtrim($projectPath, \DIRECTORY_SEPARATOR);
        $this->renamedFilePath = $renamedFilePath;
    }

    public function getFileName(): string
    {
        return \basename($this->filePath);
    }

    public function getRelativePath(): string
    {
        $projectPath = $this->projectPath . \DIRECTORY_SEPARATOR;

        $relativePath = \substr_replace($this->filePath, '', 0, \mb_strlen($projectPath));

        return \ltrim($relativePath, \DIRECTORY_SEPARATOR);
    }

    public function getFullPath(): string
    {
        if (\file_exists($this->getCachedPath())) {
            return $this->getCachedPath();
        }

        return $this->filePath;
    }

    public function getCachedPath(): string
    {
        return $this->cachedPath;
    }

    public function getExtension(): string
    {
        return \pathinfo($this->filePath, \PATHINFO_EXTENSION);
    }

    public function getStatus(): string
    {
        return $this->fileStatus;
    }

    public function getName(): string
    {
        return $this->getRelativePath();
    }

    public function getFilePathBeforeRename(): ?string
    {
        return $this->renamedFilePath;
    }
}
